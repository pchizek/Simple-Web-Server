#include "client_http.hpp"
#include "server_http.hpp"
#include <future>

#include "../serial/serial-io.h"

// Added for the json-example
#define BOOST_SPIRIT_THREADSAFE
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

// Added for the default_resource example
#include <algorithm>
#include <boost/filesystem.hpp>
#include <fstream>
#include <vector>
#ifdef HAVE_OPENSSL
#include "crypto.hpp"
#endif

using namespace std;
// Added for the json-example:
using namespace boost::property_tree;

// Decode common characters
string encoded_string[5] = {
"%B4",
"%91",
"%92",
"%93",
"%94"
};

char decoded_char[9] = {
'`',
'\'',
'\'',
'\"',
'\"'
};

//typedef pair<string [9],char [9]> decode_pair;
//decode_pair = make_pair(encoded_string,decoded_char);

using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;

void serve_page(const shared_ptr<HttpServer::Request> &request, const shared_ptr<HttpServer::Response> &response);
void serve(const shared_ptr<HttpServer::Response> &response, const shared_ptr<ifstream> &fstr);
void url_decode(string *input_string);

int main() {
  // HTTP-server at port 8080 using 1 thread
  // Unless you do more heavy non-threaded processing in the resources,
  // 1 thread is usually faster than several threads
  HttpServer server;
  server.config.port = 8080;

  //server.resource["^/messageSent"]["POST"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request){
  server.default_resource["POST"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
    auto content = request->content.string();

    string message = (string)content;

    // Replace URL encoded characters
    url_decode(&message);

    // Find length
    int len = message.length();

    // Split String
    int senderNameStart, contentStart;
    senderNameStart = message.find("senderName=");
    contentStart = message.find("&message=");

    if((senderNameStart > len) | (contentStart > len)) {
      response->write("Error");
      return;
    }

    try {
      string sender, content;

      senderNameStart += 11;
      sender.assign(message, senderNameStart, (contentStart - senderNameStart));

      contentStart += 9;
      content.assign(message, contentStart, len - 1);

      relay_message(sender,content);

    }
    catch(const SimpleWeb::system_error &e) {
      response->write("Error");
      return;
    }

    //response->
    //serve_page(request,response);
    response->write("Message Sent!");
  };
  // Parse content

  // Default GET-example. If no other matches, this anonymous function will be called.
  // Will respond with content in the web/-directory, and its subdirectories.
  // Default file: index.html
  // Can for instance be used to retrieve an HTML 5 client that uses REST-resources on this server
  server.default_resource["GET"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
    
    try {
      serve_page(request, response);
    }
    catch(const exception &e) {
      response->write(SimpleWeb::StatusCode::client_error_bad_request, "Could not open path " + request->path + ": " + e.what());
    }

  };

  server.on_error = [](shared_ptr<HttpServer::Request> /*request*/, const SimpleWeb::error_code & /*ec*/) {
    // Handle errors here
    // Note that connection timeouts will also call this handle with ec set to SimpleWeb::errc::operation_canceled
  };

  thread server_thread([&server]() {
    // Start server
    server.start();
  });

  // Wait for server to start so that the client can connect
  this_thread::sleep_for(chrono::seconds(1));


  // Client examples
  HttpClient client("localhost:8080");

  string exampleMessageString = "senderName=pat&message=hello";

  // Synchronous request examples

  try {

    auto r2 = client.request("POST", exampleMessageString);
    cout << r2->content.rdbuf() << endl;
  }
  catch(const SimpleWeb::system_error &e) {
    cerr << "Client request error: " << e.what() << endl;
  }


  client.io_service->run();

  server_thread.join();
}

#define CHUNK_BYTES 131072

void serve(const shared_ptr<HttpServer::Response> &response, const shared_ptr<ifstream> &fstr) {

  static vector<char> buffer(CHUNK_BYTES);
  streamsize read_len;

  if((read_len = fstr->read(&buffer[0], static_cast<streamsize>(buffer.size())).gcount()) > 0) {
    response->write(&buffer[0], read_len);

    if(read_len == static_cast<streamsize>(buffer.size())) {
      response->send([response, fstr](const SimpleWeb::error_code &ec) {
        if(!ec)
          serve(response, fstr);
        else
          cerr << "Connection interrupted" << endl;
      });
    }
  }
}


void serve_page(const shared_ptr<HttpServer::Request> &request, const shared_ptr<HttpServer::Response> &response) {


  // verify file is within root_path
  auto web_root_path = boost::filesystem::canonical("web");
  auto path = boost::filesystem::canonical(web_root_path / request->path);

  // Check if path is within web_root_path
  if(distance(web_root_path.begin(), web_root_path.end()) > distance(path.begin(), path.end()) ||
     !equal(web_root_path.begin(), web_root_path.end(), path.begin()))
    throw invalid_argument("path must be within root path");
  if(boost::filesystem::is_directory(path))
    path /= "sendMessage.html";

  SimpleWeb::CaseInsensitiveMultimap header;

  // open the filestream
  auto fstr = make_shared<ifstream>();
  fstr->open(path.string(), ifstream::in | ios::binary | ios::ate);

  if(*fstr) {

    // Find lenght and set iterator ti beginning
    auto length = fstr->tellg();
    fstr->seekg(0, ios::beg);

    // Define content length
    header.emplace("Content-Length", to_string(length));
    response->write(header);

    // Send chunks of data
    serve(response, fstr);
  }
  else {
    throw invalid_argument("could not read file");
  }
}

void url_decode(string *input_string){

  int decode = 0;

  while (decode != string::npos){

    decode = input_string->find('%',decode);

    if (decode != string::npos){

      // Get substring
      string encoded = input_string->substr(decode,3); 
      
      // See if it can be interpreted as hex
      for (uint8_t i=0;i<5;i++){

        // If the substring matches, replace with a character
        if (encoded.compare(encoded_string[i]) == 0){
          input_string->replace(decode,3,1,decoded_char[i]);
          continue;
        }

      }
      
      /*
       * Decode Emoji and display solid block 
       *
       * Encoded Emojis are interpreted with 3 different start codes, with the exception of enclosed numbers.
       * "%C2" for the copyright and registered symbols
       * "%E2" and "%F0" for most other things
       * For enclosed numbers, we find the last three bytes which start with "%E2" and also replace the byte before.
       */
      if (encoded.compare("%C2") == 0){
        input_string->replace(decode,6,1,'_');
        continue;
      }    

      else if (encoded.compare("%E2") == 0){
        if((encoded.substr(decode+6,3)).compare("%A3") == 0){
          input_string->replace(decode-3,12,1,'_');
        }
        else {
          input_string->replace(decode,9,1,'_');
        }
        continue;
      }

      else if (encoded.compare("%F0") == 0){
        input_string->replace(decode,12,1,'_');
        continue;
      }

      // If we get to this point then we'll interpret the following numbers as hex and print the cooresponding char
      char other_decode = stoi(encoded.substr(1,2),nullptr,16);
      input_string->replace(decode,3,1,other_decode);

    }

  }


}
